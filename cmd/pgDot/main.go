package main

import (
	"context"
	"fmt"
	"strings"

	"github.com/jackc/pgx/v4"
)

type dbConstraint struct {
	constraintType string
	schema         string
	table          string
	column         string
}

type dbField struct {
	position     int
	defaultValue string
	nullable     bool
	dataType     string
	charLen      int
	generated    bool
	expression   string
	updatable    bool
	constraints  map[string]dbConstraint
}

type dbTable struct {
	fields map[string]*dbField
}

type dbSchema struct {
	tables map[string]*dbTable
}

type dbStruct struct {
	schemas map[string]*dbSchema
}

func (s *dbStruct) getField(catalogName, schemaName, tableName, fieldName string) (*dbField, bool) {
	schema, ok := s.schemas[schemaName]
	if !ok {
		return nil, false
	}

	table, ok := schema.tables[tableName]
	if !ok {
		return nil, false
	}

	field, ok := table.fields[fieldName]
	return field, ok
}

func (s *dbStruct) setField(catalogName, schemaName, tableName, fieldName string, field *dbField) {
	schema, ok := s.schemas[schemaName]
	if !ok {
		schema = &dbSchema{tables: map[string]*dbTable{}}
		s.schemas[schemaName] = schema
	}

	table, ok := schema.tables[tableName]
	if !ok {
		table = &dbTable{fields: map[string]*dbField{}}
		schema.tables[tableName] = table
	}

	table.fields[fieldName] = field
}

func sqlbool(v any) bool {
	if s, ok := v.(string); ok {
		return s == "YES"
	}
	return false
}

func sqlint(v any) int {
	switch n := v.(type) {
	case int:
		return n
	case int32:
		return int(n)
	case int64:
		return int(n)
	default:
		return 0
	}
}

func sqlstring(v any) string {
	switch s := v.(type) {
	case string:
		return s
	default:
		return ""
	}
}

func fetchTables(ctx context.Context, conn *pgx.Conn) dbStruct {
	fmt.Printf("fetching...\n")
	rows, err := conn.Query(ctx, `
		SELECT cols.table_catalog,
			cols.table_schema,
			cols.table_name,
			cols.column_name,
			cols.ordinal_position,
			cols.column_default,
			cols.is_nullable,
			cols.data_type,
			cols.character_maximum_length,
			cols.is_generated,
			cols.generation_expression,
			cols.is_updatable,
			constraints.constraint_name,
			constraints.constraint_type,
			constraints.foreign_table_schema,
			constraints.foreign_table_name,
			constraints.foreign_column_name
		FROM information_schema.columns AS cols
		LEFT JOIN (SELECT tc.table_catalog,
							tc.table_schema,
							tc.constraint_name,
							tc.table_name,
							tc.constraint_type,
							kcu.column_name,
							ccu.table_schema AS foreign_table_schema,
							ccu.table_name AS foreign_table_name,
							ccu.column_name AS foreign_column_name
					FROM information_schema.table_constraints AS tc
					JOIN information_schema.key_column_usage AS kcu
						ON kcu.constraint_schema = tc.constraint_schema
							AND kcu.constraint_name = tc.constraint_name
					JOIN information_schema.constraint_column_usage AS ccu
						ON ccu.constraint_schema = tc.constraint_schema
							AND ccu.constraint_name = tc.constraint_name) AS constraints
			ON constraints.table_catalog = cols.table_catalog
				AND constraints.table_schema = cols.table_schema
				AND constraints.table_name = cols.table_name
				AND constraints.column_name = cols.column_name
		WHERE cols.table_schema NOT IN ('pg_catalog','information_schema')
		ORDER BY cols.table_catalog,
				cols.table_schema,
				cols.table_name,
				cols.ordinal_position,
				constraints.constraint_name;`)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	structure := dbStruct{schemas: map[string]*dbSchema{}}

	for rows.Next() {
		row, err := rows.Values()
		if err != nil {
			panic(err)
		}
		fld, ok := structure.getField(row[0].(string), row[1].(string), row[2].(string), row[3].(string))
		if !ok {
			fld = &dbField{
				position:     sqlint(row[4]),
				defaultValue: sqlstring(row[5]),
				nullable:     sqlbool(row[6]),
				dataType:     sqlstring(row[7]),
				charLen:      sqlint(row[8]),
				generated:    sqlbool(row[9]),
				expression:   sqlstring(row[10]),
				updatable:    sqlbool(row[11]),
			}
			structure.setField(row[0].(string), row[1].(string), row[2].(string), row[3].(string), fld)
		}
	}

	return structure
}

func (s dbStruct) String() string {
	b := strings.Builder{}
	for schemaName, schema := range s.schemas {
		fmt.Fprintf(&b, "schema %s:\n", schemaName)
		for tableName, table := range schema.tables {
			fmt.Fprintf(&b, "table %s.%s:\n", schemaName, tableName)
			for fieldName, field := range table.fields {
				fmt.Fprintf(&b, "\t%d %s\t%s\n", field.position, fieldName, field.dataType)
			}
		}
	}
	return b.String()
}

func main() {
	ctx := context.Background()
	db, err := pgx.Connect(ctx, "host=localhost user=bake dbname=bake password=bake sslmode=disable")
	if err != nil {
		panic(err)
	}
	defer db.Close(ctx)

	err = db.Ping(ctx)
	if err != nil {
		panic(err)
	}

	fmt.Println("connected")

	structure := fetchTables(ctx, db)
	fmt.Printf("%v\n", structure)

	{
		rows, err := db.Query(ctx, `select 2 + 2`)
		if err != nil {
			panic(err)
		}
		defer rows.Close()

		for rows.Next() {
			var res int64

			err = rows.Scan(&res)
			if err != nil {
				panic(err)
			}
			fmt.Println("res: ", res)
		}
	}

	{
		stmt, err := db.Prepare(ctx, "fordemo", "select 4 + $1")
		fmt.Printf("prepared statement: %v\n", stmt)
		if err != nil {
			panic(err)
		}

		rows, err := db.Query(ctx, "fordemo", 7)
		if err != nil {
			panic(err)
		}
		defer rows.Close()

		for rows.Next() {
			var res int64

			err = rows.Scan(&res)
			if err != nil {
				panic(err)
			}
			fmt.Println("res: ", res)
		}
	}
}
