-- Write your migrate up statements here

CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    full_name character varying NOT NULL,
    phone character varying,
    avatar character varying,
    email character varying NOT NULL DEFAULT ''::character varying,
    encrypted_password character varying NOT NULL DEFAULT ''::character varying,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    category_counts jsonb,
    created_at timestamp without time zone NOT NULL DEFAULT NOW(),
    updated_at timestamp without time zone NOT NULL DEFAULT NOW()
);

-- Indices -------------------------------------------------------

CREATE UNIQUE INDEX index_users_on_email ON users(email text_ops);
CREATE UNIQUE INDEX index_users_on_reset_password_token ON users(reset_password_token text_ops);


---- create above / drop below ----
-- Write your migrate up statements here

CREATE TABLE products (
    id BIGSERIAL PRIMARY KEY,
    name          text CHECK (length(name) > 1 AND length(name) < 50),
    description   text,
    price         numeric(7,2),
    tags          text[],
    category_ids  bigint[] NOT NULL DEFAULT ARRAY[]::bigint[],
    user_id       bigint REFERENCES users(id),
    created_at    timestamp without time zone NOT NULL DEFAULT NOW(),
    updated_at    timestamp without time zone NOT NULL DEFAULT NOW(),

    -- tsvector column needed for full-text search
    tsv tsvector GENERATED ALWAYS
      AS (to_tsvector('english', name) || to_tsvector('english', description)) STORED
);

-- Indices -------------------------------------------------------

CREATE INDEX index_products_on_tsv ON products USING GIN (tsv tsvector_ops);
CREATE INDEX index_products_on_user_id ON products(user_id int8_ops);
CREATE INDEX product_name_autocomplete_index ON products(name text_pattern_ops);

---- create above / drop below ----
-- Write your migrate up statements here

CREATE TABLE customers (
    id BIGSERIAL PRIMARY KEY,
    full_name character varying NOT NULL,
    phone character varying,
    stripe_id character varying,
    email character varying NOT NULL DEFAULT ''::character varying,
    encrypted_password character varying NOT NULL DEFAULT ''::character varying,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL DEFAULT NOW(),
    updated_at timestamp without time zone NOT NULL DEFAULT NOW()
);

-- Indices -------------------------------------------------------

CREATE UNIQUE INDEX index_customers_on_email ON customers(email text_ops);
CREATE UNIQUE INDEX index_customers_on_reset_password_token ON customers(reset_password_token text_ops);

---- create above / drop below ----
-- Write your migrate up statements here

CREATE TABLE purchases (
    id BIGSERIAL PRIMARY KEY,
    customer_id bigint REFERENCES customers(id),
    product_id bigint REFERENCES products(id),
    quantity integer,
    returned_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL DEFAULT NOW(),
    updated_at timestamp without time zone NOT NULL DEFAULT NOW()
);

-- Indices -------------------------------------------------------

CREATE INDEX index_purchases_on_customer_id ON purchases(customer_id int8_ops);
CREATE INDEX index_purchases_on_product_id ON purchases(product_id int8_ops);

---- create above / drop below ----
-- Write your migrate up statements here

CREATE TABLE notifications (
    id BIGSERIAL PRIMARY KEY,
    key character varying,
    subject_type character varying,
    subject_id bigint,
    user_id bigint REFERENCES users(id),
    created_at timestamp without time zone NOT NULL DEFAULT NOW(),
    updated_at timestamp without time zone NOT NULL DEFAULT NOW()
);

-- Indices -------------------------------------------------------

CREATE INDEX index_notifications_on_subject_id ON notifications(subject_id int8_ops);
CREATE INDEX index_notifications_on_user_id ON notifications(user_id int8_ops);

---- create above / drop below ----
-- Write your migrate up statements here

CREATE TABLE categories (
  id BIGSERIAL PRIMARY KEY,

  name          text NOT NULL           CHECK (length(name) < 100),
  description   text                    CHECK (length(description) < 300),

  created_at timestamp without time zone NOT NULL DEFAULT NOW(),
  updated_at timestamp without time zone NOT NULL DEFAULT NOW()
);

---- create above / drop below ----
-- Write your migrate up statements here

CREATE TABLE comments (
    id BIGSERIAL PRIMARY KEY,
    body text CHECK (length(body) > 1 AND length(body) < 200),

    product_id     bigint REFERENCES products(id),
    user_id         bigint REFERENCES users(id),
    reply_to_id     bigint REFERENCES comments(id),

    created_at timestamp without time zone NOT NULL DEFAULT NOW(),
    updated_at timestamp without time zone NOT NULL DEFAULT NOW()
);

---- create above / drop below ----
-- Write your migrate up statements here

CREATE TABLE chats (
    id BIGSERIAL PRIMARY KEY,
    body text,

    reply_to_id     bigint[],

    created_at timestamp without time zone NOT NULL DEFAULT NOW(),
    updated_at timestamp without time zone NOT NULL DEFAULT NOW()
);

---- create above / drop below ----
